
# softek-project

# Instrucciones Generales

 - clonar proyecto 
 	- `git clone https://gitlab.com/zilver21/softek-project.git`
 
 - dentro de la carpeta clonada, crear ambiente virtual y activarla
 	- `virtualenv venv`  
 	- `venv\Scripts\activate`

 - instalar librerias del proyecto
    - `pip install -r requirements.txt`

 - crear migración
 	- `python manage.py migrate`
 		- esta instrucción creará los modelos necesarios para los 3 problemas, hará la carga inicial de datos, y creará un superusuario para las pruebas:
 			- usuario: `admin`
 			- contrasñe: `admin`


## Correr el proyecto
 - `python manage.py runserver`
 	 Con esta instrucción se levantará el proyecto django, en `localhost:8000/` sino está ocupado el puerto.

## Links de Interés
 - `http://localhost:8000/admin` 	 
	Se habilitó la parte de administración de django para usarse como CRUD de datos, aunque ya cuenta con datos iniciales:

 - `http://localhost:8000/`
 	Se habilitó la raíz del proyecto como el API Root, ahi están los 3 links a las apis de cada problema

 - `http://localhost:8000/customer-orders/`
 	La solución al problema de status de ordenes, además de servir como endpoint para CRUD de ordenes de clientes, muestra el listado completo de las ordenes de compra, además de un campo nuevo, donde muestra el estado general de la misma compra aplicando las reglas señaladas

 - `http://localhost:8000/orders/`
 	La solución al problema de las estaciones del año, se agregó un nuevo campo en la api que es readonly solo para mostrar la estación. El link además sirve como endpoint para CRUD de más ordenes a registrar o modificar la información existente.

  - `http://localhost:8000/weathers/`
  	El link sirve como CRUD para registrar información sobre el problema de las lluvias, acepta una fecha y un booleano por registro.

  	- `http://localhost:8000/weathers/changes/`
  	  Es la solución al problema de los cambios de clima, muestra un listado con solo las fechas que tuvieron cambios de clima, es decir donde un día antes no llovío, pero esa fecha sí.


 Las soluciones se aplicaron desde los modelos, para contar con los resultados al momento de crear serializers y viewsets, se creó un solo router para las 3 apps.


