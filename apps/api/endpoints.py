from rest_framework import routers
from django.urls import path
from django.conf.urls import include

from apps.orders.views import CustomerOrderViewSet
from apps.seasons.views import OrderViewSet
from apps.weather.views import WeatherViewSet

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'customer-orders', CustomerOrderViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'weathers', WeatherViewSet)

urlpatterns = [
    path('', include(router.urls)),
]