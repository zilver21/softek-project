# STATUSES: PENDING(0), SHIPPED(1), CANCELLED(2)
data = [
	{'order_number':'ORD_1567','item_name':'LAPTOP','status':1},
	{'order_number':'ORD_1567','item_name':'MOUSE','status':1},
	{'order_number':'ORD_1567','item_name':'KEYBOARD','status':0},
	{'order_number':'ORD_1234','item_name':'GAME','status':1},
	{'order_number':'ORD_1234','item_name':'BOOK','status':2},
	{'order_number':'ORD_1234','item_name':'BOOK','status':2},
	{'order_number':'ORD_9834','item_name':'SHIRT','status':1},
	{'order_number':'ORD_9834','item_name':'PANTS','status':2},
	{'order_number':'ORD_7654','item_name':'TV','status':2},
	{'order_number':'ORD_7654','item_name':'DVD','status':2},
]