from django.db import models

# Create your models here.


class CustomerOrder(models.Model):
	PENDING, SHIPPED, CANCELLED = range(3)
	STATUSES = (
		(PENDING, 'Pending'),		
		(SHIPPED, 'Shipped'),		
		(CANCELLED, 'Cancelled'),
	)
	order_number = models.CharField(max_length=20)
	item_name = models.CharField(max_length=20)
	status = models.PositiveIntegerField(choices=STATUSES, default=PENDING)

	class Meta:
		verbose_name = 'Costomer Order'
		verbose_name_plural = 'Costomer Orders'

	def __str__(self):
		return f"{self.order_number}- {self.item_name}: {self.get_status_display()}"


	def order_status(self):
		orders = CustomerOrder.objects.filter(order_number=self.order_number)
		statuses = set([order.status for order in orders])
		if len(statuses) == 1:
			status = self.get_status_display()
		elif CustomerOrder.PENDING in statuses:
			status = 'Pending'
		else:
			status = 'Shipped' 

		return status

