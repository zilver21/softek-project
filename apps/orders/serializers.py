from rest_framework import serializers

from .models import CustomerOrder



class CustomerOrderSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()

    class Meta:
        model = CustomerOrder
        fields = [
            'order_number',
            'item_name',
            'status',
            'order_status',
        ]

    def get_status(self, obj):
        return obj.get_status_display()
