from .models import CustomerOrder
from .serializers import CustomerOrderSerializer
from rest_framework import viewsets


# ViewSets define the view behavior.
class CustomerOrderViewSet(viewsets.ModelViewSet):
    queryset = CustomerOrder.objects.all()
    serializer_class = CustomerOrderSerializer
    