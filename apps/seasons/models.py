from django.db import models
from datetime import date

# Create your models here.


class Order(models.Model):
    order_id = models.CharField(max_length=20, unique=True)
    ord_dt = models.DateField()
    qt_ordd = models.PositiveIntegerField()

    class Meta:
        verbose_name = 'order'
        verbose_name_plural = 'orders'

    def __str__(self):
        return f"{self.order_id}: {self.ord_dt}"

    
    def season(self):
        year = self.ord_dt.year
        if date(year,  3, 19) <= self.ord_dt <=  date(year,  6, 19):      
            season = 'spring'
        elif date(year,  6, 20) <= self.ord_dt <=  date(year,  9, 21):      
            season = 'summer'
        elif date(year,  9, 23) <= self.ord_dt <=  date(year, 12, 20):
            season = 'fall'
        else:
            season = 'winter'

        return season

        
        