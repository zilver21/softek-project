from rest_framework import serializers

from .models import Order



class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = [
            'order_id',
            'ord_dt',
            'qt_ordd',
            'season',
        ]
