from .models import Order
from .serializers import OrderSerializer
from rest_framework import viewsets


# ViewSets define the view behavior.
class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    