from datetime import date

data = [
	{'date':date(2020,1,1),'was_rainy':False},	     
	{'date':date(2020,1,2),'was_rainy':True},	     
	{'date':date(2020,1,3),'was_rainy':True},	     
	{'date':date(2020,1,4),'was_rainy':False},	     
	{'date':date(2020,1,5),'was_rainy':False},	     
	{'date':date(2020,1,6),'was_rainy':True},	     
	{'date':date(2020,1,7),'was_rainy':False},	     
	{'date':date(2020,1,8),'was_rainy':True},	     
	{'date':date(2020,1,9),'was_rainy':True},	     
	{'date':date(2020,1,10),'was_rainy':True},	     
]
	