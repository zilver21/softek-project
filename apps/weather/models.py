from django.db import models

# Create your models here.


class Weather(models.Model):
	date = models.DateField()
	was_rainy = models.BooleanField(default=False)


	class Meta:
		verbose_name = 'Weather'
		verbose_name_plural = 'Weathers'
		ordering = ['date']

	def __str__(self):
		return f"{self.date}: {self.was_rainy}"
