from .models import Weather
from .serializers import WeatherSerializer
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

# ViewSets define the view behavior.
class WeatherViewSet(viewsets.ModelViewSet):
    queryset = Weather.objects.all()
    serializer_class = WeatherSerializer
    
    @action(methods=['get'], detail=False)
    def changes(self, request, pk=None):
        days = Weather.objects.all()
        prev = False
        actual = []
        for day in days:
            if not prev and day.was_rainy:
                actual.append(day)

            prev = day.was_rainy

        serializer = self.get_serializer(actual,many=True)
        return Response(serializer.data)